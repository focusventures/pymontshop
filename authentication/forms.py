from django import forms 
from authentication.models import CustomUser
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from crispy_forms.layout import (Layout, Fieldset, Field,
                                 ButtonHolder, Submit, Div)
from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import Tab, TabHolder
# from .models import CustomUser


class ProfileForm(forms.ModelForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)
    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=30,
        required=False)
    
    email = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'form-control'}),
        max_length=75,
        required=False)
    phone_number = forms.RegexField(regex=r'^\+?1?\d{9,15}$',
                                    widget=forms.TextInput(
                                        attrs={'class': 'form-control', 'placeholder': 'phone number'}),
                                        help_text=(
                                        "Phone number must be entered in the format: ''. Up to 15 digits allowed."))


    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name',
                  'email', 'phone_number', 'avatar']

    def __init__(self, *args, **kwargs):
        super(ProfileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Post Info',
                   
                    'username',
                    'first_name',
                    'last_name',
                    'middle_name',
                    'email',
                    'phone_number',
                    'avatar',
                  
                  
                    )
           
            ),
            ButtonHolder(
                Submit('submit', 'Update',
                       css_class='float-right btn-dark mr-3')
            )
        )


class ChangePasswordForm(forms.ModelForm):
    id = forms.CharField(widget=forms.HiddenInput())
    old_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Old password",
        required=True)

    new_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="New password",
        required=True)
    confirm_password = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        label="Confirm new password",
        required=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'old_password', 'new_password', 'confirm_password']

    def clean(self):
        super(ChangePasswordForm, self).clean()
        old_password = self.cleaned_data.get('old_password')
        new_password = self.cleaned_data.get('new_password')
        confirm_password = self.cleaned_data.get('confirm_password')
        id = self.cleaned_data.get('id')
        user = CustomUser.objects.get(pk=id)
        if not user.check_password(old_password):
            self._errors['old_password'] = self.error_class([
                'Old password don\'t match'])
        if new_password and new_password != confirm_password:
            self._errors['new_password'] = self.error_class([
                'Passwords don\'t match'])
        return self.cleaned_data


    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Change Password',
                   
                    'old_password',
                    'new_password',
                    'id',
                    'confirm_password',
                
                  
                  
                    )
           
            ),
            ButtonHolder(
                Submit('submit', 'Update',
                       css_class='float-right btn-dark mr-3')
            )
        )



class SignUpForm(forms.ModelForm):
    # phone_number = forms.CharField(
    #     widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
    #     max_length=10,
    #     required=False)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}), label='Username')
    phone_number = forms.RegexField(regex=r'^\d{10}$',widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'phone number'}),
                                    help_text=(
                                        "Phone number must be entered in the format: '0721XXXXXX'."))
     # noqa: E261
    password = forms.CharField(
                                   widget=forms.PasswordInput(
                                       attrs={'class': 'form-control', 'placeholder': 'password'}))
    confirm_password = forms.CharField(
                                           widget=forms.PasswordInput(
                                               attrs={'class': 'form-control', 'placeholder': 'confirm password'}),
                                           label="Confirm your password",
                                           required=True)

    class Meta:
            model = CustomUser
            exclude = ['last_login', 'date_joined']
            fields = ['username', 'phone_number', 'email',  'password', 'confirm_password'  ]

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = Layout(
            TabHolder(
                Tab('Sign Up',
                   
                    'username',
                    'phone_number',
                    'email',
                    'password',
                    'confirm_password',
                
                  
                    )
           
            ),
            ButtonHolder(
                Submit('submit', 'Signup',
                       css_class='float-right btn-dark mr-3')
            )
        )



    def clean(self):
            super(SignUpForm, self).clean()
            password = self.cleaned_data.get('password')
            confirm_password = self.cleaned_data.get('confirm_password')
            if password and password != confirm_password:
                self._errors['password'] = self.error_class(
                    ['Passwords don\'t match'])
            return self.cleaned_data




class CustomUserCreationForm(UserCreationForm):
        """
        A form that creates a user, with no privileges, from the given email and
        password.
        """

        def __init__(self, *args, **kargs):
            super(CustomUserCreationForm, self).__init__(*args, **kargs)
            del self.fields['phone_number']

        class Meta:
            model = CustomUser
            fields = ("phone_number",)

class CustomUserChangeForm(UserChangeForm):
        """A form for updating users. Includes all the fields on
        the user, but replaces the password field with admin's
        password hash display field.
        """

        def __init__(self, *args, **kargs):
            super(CustomUserChangeForm, self).__init__(*args, **kargs)
            del self.fields['phone_number']

        class Meta:
            model = CustomUser
            exclude = ('is_staff',)




