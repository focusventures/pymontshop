from django.urls import path

from . import views



urlpatterns = [
     path('signup', views.signup, name="signup"),
     path('login', views.login, name="login" ),
     path('logout', views.user_logout, name="logout" ),
     path('confirm', views.confirm, name="confirm" ),
     path('reset', views.reset, name="reset" ),
     path('profile', views.profile, name="profile" ),
]
