from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserChangeForm, CustomUserCreationForm
from .models import  CustomUser



class CustomUserAdmin(admin.ModelAdmin):
    list_display = (
        'phone_number', 'username', 'email', 'first_name', 'last_name', 'date_joined'
    )
    search_fields = (
        'phone_number','username', 'email', 'first_name', 'last_name', 'date_joined'
    )

admin.site.register(CustomUser, CustomUserAdmin)